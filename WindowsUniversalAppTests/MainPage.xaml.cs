﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WindowsUniversalAppTests
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            //
            // Formatting text with inline elements
            //  see https://msdn.microsoft.com/en-us/windows/uwp/controls-and-patterns/text-block#formatting-text
            // and
            // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.documents.run.aspx
            // https://stackoverflow.com/questions/12727491/programmatically-set-textblock-foreground-color
            //
            Run r = new Run();
            r.Text = "\nHello World!";
            r.Foreground = new SolidColorBrush(Colors.Red);
            r.FontSize = 14;
            // https://msdn.microsoft.com/en-us/library/windows/apps/windows.ui.text.fontweights
            r.FontWeight = FontWeights.Bold;
            SSHView.Inlines.Add(r);

            //SSHView.Text += "\nLore ipsum!";
            r = new Run();
            r.Text = " Lore ipsum!";
            r.FontSize = 14;
            SSHView.Inlines.Add(r);

            Run r2 = new Run();
            r2.Text = "\n Text.........";
            r2.Foreground = new SolidColorBrush(Colors.Green);
            r2.FontSize = 14;
            SSHView.Inlines.Add(r2);

            //
            // A span can be used for grouping multiple Run elements
            // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.documents.span.aspx
            //
            Span mySpan = new Span();
            for (int i = 0; i < 10; i++)
            {
                r = new Run();
                r.Text = "\nHello " + i;
                r.FontSize = 14;
                r.Foreground = new SolidColorBrush(Colors.Blue);
                r.FontWeight = FontWeights.Bold;
                mySpan.Inlines.Add(r);
            }
            SSHView.Inlines.Add(mySpan);

            //
            // https://social.msdn.microsoft.com/Forums/en-US/915740e9-a693-40b9-96a4-07d2784d3b53/textblock-underline-in-winrt?forum=winappswithcsharp
            //
            Underline ul = new Underline();
            r = new Run();
            r.Text = "\nHere is an underlined text";
            ul.Inlines.Add(r);
            SSHView.Inlines.Add(ul);

            //
            // check if same SolidColorBrush can be used more than once
            //
            SolidColorBrush greenBrush = new SolidColorBrush(Colors.Green);
            for (int i = 0; i < 10; i++)
            {
                r = new Run();
                r.Text = "\nblblblblblbl " + i;
                r.FontSize = 14;
                r.Foreground = greenBrush;
                SSHView.Inlines.Add(r);
            }

            //
            // a Commandbar test
            // see https://msdn.microsoft.com/en-us/windows/uwp/controls-and-patterns/app-bars
            //
            // https://msdn.microsoft.com/library/windows/apps/windows.ui.xaml.controls.page.bottomappbar.aspx
            // https://msdn.microsoft.com/library/windows/apps/xaml/windows.ui.xaml.controls.appbarbutton.aspx
            // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.controls.iconelement.aspx
            // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.controls.symbol.aspx
            //
            // http://unicode-table.com/en
            //
            CommandBar cmdBar = new CommandBar();
            cmdBar.ClosedDisplayMode = AppBarClosedDisplayMode.Compact;

            this.mainPage.BottomAppBar = cmdBar;
            AppBarButton b = new AppBarButton();
            b.Label = "Tab";
            FontIcon f = new FontIcon();
            f.FontFamily = new FontFamily("Candara");
            f.Glyph = "\u21b9";
            b.Icon = f;


            AppBarButton b1 = new AppBarButton();
            b1.Label = "Up";
            f = new FontIcon();
            f.FontFamily = new FontFamily("Candara");
            f.Glyph = "\u2191";
            b1.Icon = f;

            AppBarButton b2 = new AppBarButton();
            b2.Label = "Down";
            f = new FontIcon();
            f.FontFamily = new FontFamily("Candara");
            f.Glyph = "\u2193";
            b2.Icon = f;

            AppBarButton b3 = new AppBarButton();
            b3.Label = "Repeat";
            b3.Icon = new SymbolIcon(Symbol.RepeatAll);

            cmdBar.PrimaryCommands.Add(b);
            cmdBar.PrimaryCommands.Add(b1);
            cmdBar.PrimaryCommands.Add(b2);
            cmdBar.PrimaryCommands.Add(b3);

            //
            //
            //
            CultureInfo culture = new CultureInfo("de-DE");
            DateTime localDate = DateTime.Now;
            System.Diagnostics.Debug.WriteLine(localDate.ToString(culture));
        }
    }
}
