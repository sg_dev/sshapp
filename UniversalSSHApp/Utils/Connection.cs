﻿

using System;

namespace UniversalSSHApp.Utils
{
    public class Connection : IEquatable<Connection>
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public bool Equals(Connection other)
        {
            return this.Host.Equals(other.Host) && this.Port == other.Port
                && this.Username.Equals(other.Username)
                && this.Password.Equals(other.Password);
        }
    }    
}
