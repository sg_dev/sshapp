﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.DataProtection;
using Windows.Storage;
using Windows.Storage.Streams;

namespace UniversalSSHApp.Utils
{
    // most methods inspired from
    // https://msdn.microsoft.com/windows/uwp/security/microsoft-passport-login
    public static class ConnectionHelper
    {
        private const string CONNECTION_LIST_FILE_NAME = "connectionlist.db";
        private static string _connectionListPath =
            Path.Combine(ApplicationData.Current.LocalFolder.Path, CONNECTION_LIST_FILE_NAME);
        private static List<Connection> connectionList = new List<Connection>();

        public static async void SaveConnectionsListAsync()
        {
            LoggingHelper.writeLogEntryLine(typeof(ConnectionHelper).Name, 
                "Saving data to " + _connectionListPath);

            string connectionXml = SerializeConnectionListToXml();
            // encrypt data
            IBuffer data = await SampleProtectAsync(connectionXml);

            StorageFile connectionFile = null;
            if(File.Exists(_connectionListPath))
            {
                connectionFile = await StorageFile.GetFileFromPathAsync(_connectionListPath);
                await FileIO.WriteBufferAsync(connectionFile, data);
            }
            else
            {
                connectionFile = await ApplicationData.Current.
                    LocalFolder.CreateFileAsync(CONNECTION_LIST_FILE_NAME);
            }
            await FileIO.WriteBufferAsync(connectionFile, data);
        }

        public static async Task<List<Connection>> LoadConnectionListAsync()
        {
            LoggingHelper.writeLogEntryLine(typeof(ConnectionHelper).Name,
                "LoadConnectionListAsync called using _connectionListPath: " +
                _connectionListPath);

            if (File.Exists(_connectionListPath))
            {
                StorageFile connectionsFile = await StorageFile.GetFileFromPathAsync(_connectionListPath);

                // decrypt data
                IBuffer data = await FileIO.ReadBufferAsync(connectionsFile);
                string xml = await SampleUnprotectData(data);
                DeserializeXmlToConnectionList(xml);
            }

            return connectionList;
        }

        public static Connection AddConnection(string host, string port, string username, string password)
        {
            Connection con = new Connection();
            con.Host = host;

            try
            {
                con.Port = Int32.Parse(port);
            }
            catch (Exception ex)
            {
                if (ex is FormatException || ex is ArgumentOutOfRangeException)
                {
                    LoggingHelper.writeLogEntryLine(typeof(ConnectionHelper).Name, 
                        "Port given was wrong!");
                    throw new ApplicationException("ERROR: Port given was wrong!", ex);
                }

                LoggingHelper.writeLogEntryLine(typeof(ConnectionHelper).Name, 
                    "Exception: " + ex);
                throw;
            }

            con.Username = username;
            con.Password = password;

            // TODO how to handle same host/port/username but different passwords
            if(!connectionList.Contains(con))
            {
                connectionList.Add(con);
            }
            else
            {
                LoggingHelper.writeLogEntryLine(typeof(ConnectionHelper).Name, 
                    "Connection was not saved since it is already contained in the db!");
            } 
            
            // don't save it since we don't know if data is correct
            // -> only if connection succeeds
            //System.Diagnostics.Debug.WriteLine("Now the connection data will be saved!");
            //SaveConnectionsListAsync();
            return con;
        }

        public static bool RemoveConnection(Connection con)
        {
            bool result = connectionList.Remove(con);
            if(result == true)
            {
                SaveConnectionsListAsync();
            }

            return result;
        }

        public static List<Connection> getConnectionList()
        {
            return connectionList;
        }


        private static string SerializeConnectionListToXml()
        {
            XmlSerializer xmlizer = new XmlSerializer(typeof(List<Connection>));
            StringWriter writer = new StringWriter();
            xmlizer.Serialize(writer, connectionList);

            return writer.ToString();
        }

        private static List<Connection> DeserializeXmlToConnectionList(string listAsXml)
        {
            XmlSerializer xmlizer = new XmlSerializer(typeof(List<Connection>));
            TextReader textreader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(listAsXml)));

            return connectionList = (xmlizer.Deserialize(textreader)) as List<Connection>;
        }

        //
        // https://msdn.microsoft.com/en-us/windows/uwp/security/data-protection
        //
        private static async Task<IBuffer> SampleProtectAsync(
            String strMsg,
            String strDescriptor = "LOCAL=user",
            BinaryStringEncoding encoding = BinaryStringEncoding.Utf8)
        {
            // Create a DataProtectionProvider object for the specified descriptor.
            DataProtectionProvider Provider = new DataProtectionProvider(strDescriptor);

            // Encode the plaintext input message to a buffer.
            encoding = BinaryStringEncoding.Utf8;
            IBuffer buffMsg = CryptographicBuffer.ConvertStringToBinary(strMsg, encoding);

            // Encrypt the message.
            IBuffer buffProtected = await Provider.ProtectAsync(buffMsg);

            // Execution of the SampleProtectAsync function resumes here
            // after the awaited task (Provider.ProtectAsync) completes.
            return buffProtected;
        }

        // https://msdn.microsoft.com/en-us/windows/uwp/security/data-protection
        private static async Task<String> SampleUnprotectData(
            IBuffer buffProtected,
            BinaryStringEncoding encoding = BinaryStringEncoding.Utf8)
        {
            // Create a DataProtectionProvider object.
            DataProtectionProvider Provider = new DataProtectionProvider();

            // Decrypt the protected message specified on input.
            IBuffer buffUnprotected = await Provider.UnprotectAsync(buffProtected);

            // Execution of the SampleUnprotectData method resumes here
            // after the awaited task (Provider.UnprotectAsync) completes
            // Convert the unprotected message from an IBuffer object to a string.
            String strClearText = CryptographicBuffer.ConvertBinaryToString(encoding, buffUnprotected);

            // Return the plaintext string.
            return strClearText;
        }
    }
}
