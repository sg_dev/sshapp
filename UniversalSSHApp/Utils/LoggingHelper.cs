﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace UniversalSSHApp.Utils
{
    class LoggingHelper
    {
#if DEBUG
        private static ILogging logging = new ConsoleLogger();
#else
        private static ILogging logging = new ConsoleLogger();
        //
        // Bug with log file creation: System.UnauthorizedException
        // doesnt work on device
        // maybe use other dir on phone - documents etc. may be better
        //
        //private static ILogging logging = new FileLogger();
#endif

        public static void writeLogEntryLine(string data)
        {
            logging.writeLogEntryLine(data);
        }

        public static void writeLogEntryLine(string className, string data)
        {
            logging.writeLogEntryLine(className, data);
        }
    }

    abstract class ILogging
    {
        private CultureInfo culture;

        public ILogging()
        {
            this.culture = new CultureInfo("de-DE");
        }

        abstract public void writeLogEntryLine(string className, string data);
        abstract public void writeLogEntryLine(string data);

        protected string createGenericString(string data)
        {
            DateTime localDate = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            builder.Append("[");
            builder.Append(DateTime.Now.ToString(this.culture));
            builder.Append("] ");
            builder.Append(data);
            builder.Append("\n");

            return builder.ToString();
        }

        protected string createGenericString(string className, string data)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[");
            builder.Append(DateTime.Now.ToString(this.culture));
            builder.Append(": ");
            builder.Append(className);
            builder.Append("] ");
            builder.Append(data);
            builder.Append("\n");

            return builder.ToString();
        }
    }

    class FileLogger : ILogging
    {
        private const string LOG_FILE_NAME = "log.txt";
        private static string _logfilePath =
            Path.Combine(ApplicationData.Current.LocalFolder.Path, LOG_FILE_NAME);
        private StorageFile logFile;

        public FileLogger()
        {
        }

        public override async void writeLogEntryLine(string data)
        {
            if (this.logFile == null)
            {
                await this.getOrCreateLogFile();
            }

            await FileIO.AppendTextAsync(logFile, this.createGenericString(data));
        }

        public override async void writeLogEntryLine(string className, string data)
        {
            if (this.logFile == null)
            {
                await this.getOrCreateLogFile();
            }

            await FileIO.AppendTextAsync(logFile, this.createGenericString(className, data));
        }

        private async Task getOrCreateLogFile()
        {
            if (File.Exists(_logfilePath))
            {
                this.logFile = await StorageFile.GetFileFromPathAsync(_logfilePath);

            }
            else
            {
                this.logFile = await ApplicationData.Current.
                    LocalFolder.CreateFileAsync(LOG_FILE_NAME);
            }
        }
    }

    class ConsoleLogger : ILogging
    {
        public override void writeLogEntryLine(string data)
        {
            System.Diagnostics.Debug.Write(this.createGenericString(data));
        }

        public override void writeLogEntryLine(string className, string data)
        {
            System.Diagnostics.Debug.Write(this.createGenericString(className, data));
        }
    }
}
