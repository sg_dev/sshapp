﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UniversalSSHApp.Utils
{
    namespace Ansi
    {
        class Style
        {
            public static readonly string RESET = "reset";
            public static readonly string BOLD = "bold";
            public static readonly string ITALIC = "italic";
            public static readonly string UNDERLINE = "unterline";
            public static readonly string BLACK = "black";
            public static readonly string RED = "red";
            public static readonly string GREEN = "green";
            public static readonly string YELLOW = "yellow";
            public static readonly string BLUE = "blue";
            public static readonly string MAGENTA = "magenta";
            public static readonly string CYAN = "cyan";
            public static readonly string WHITE = "white";
            public static readonly string BG_BLACK = "bg-black";
            public static readonly string BG_RED = "bg-red";
            public static readonly string BG_GREEN = "bg-green";
            public static readonly string BG_YELLOW = "bg-yellow";
            public static readonly string BG_BLUE = "bg-blue";
            public static readonly string BG_MAGENTA = "bg-magenta";
            public static readonly string BG_CYAN = "bg-cyan";
            public static readonly string BG_WHITE = "bg-white";

            private Style () { }
        }
    }

    class ANSIEscapeCodeHelper
    {
        private Dictionary<string, string> escapeseqNameMapping;
        private Dictionary<string, string> escapeCodeNameMapping;
        private Dictionary<string, bool> nameActivatedMapping;
        private Regex ansiEscapeseqPattern;
        private Regex oscPatter;

        public ANSIEscapeCodeHelper()
        {
            // really needed???
            this.escapeseqNameMapping = new Dictionary<string, string>();
            this.escapeseqNameMapping.Add("\x1b[0m", Ansi.Style.RESET);
            this.escapeseqNameMapping.Add("\x1b[1m", Ansi.Style.BOLD);
            this.escapeseqNameMapping.Add("\x1b[3m", Ansi.Style.ITALIC);
            this.escapeseqNameMapping.Add("\x1b[4m", Ansi.Style.UNDERLINE);
            this.escapeseqNameMapping.Add("\x1b[30m", Ansi.Style.BLACK);
            this.escapeseqNameMapping.Add("\x1b[31m", Ansi.Style.RED);
            this.escapeseqNameMapping.Add("\x1b[32m", Ansi.Style.GREEN);
            this.escapeseqNameMapping.Add("\x1b[33m", Ansi.Style.YELLOW);
            this.escapeseqNameMapping.Add("\x1b[34m", Ansi.Style.BLUE);
            this.escapeseqNameMapping.Add("\x1b[35m", Ansi.Style.MAGENTA);
            this.escapeseqNameMapping.Add("\x1b[36m", Ansi.Style.CYAN);
            this.escapeseqNameMapping.Add("\x1b[37m", Ansi.Style.WHITE);
            this.escapeseqNameMapping.Add("\x1b[40m", Ansi.Style.BG_BLACK);
            this.escapeseqNameMapping.Add("\x1b[41m", Ansi.Style.BG_RED);
            this.escapeseqNameMapping.Add("\x1b[42m", Ansi.Style.BG_GREEN);
            this.escapeseqNameMapping.Add("\x1b[43m", Ansi.Style.BG_YELLOW);
            this.escapeseqNameMapping.Add("\x1b[44m", Ansi.Style.BG_BLUE);
            this.escapeseqNameMapping.Add("\x1b[45m", Ansi.Style.BG_MAGENTA);
            this.escapeseqNameMapping.Add("\x1b[46m", Ansi.Style.BG_CYAN);
            this.escapeseqNameMapping.Add("\x1b[47m", Ansi.Style.BG_WHITE);
            // for the reset the '0' can also be omitted
            this.escapeseqNameMapping.Add("\x1b[m", Ansi.Style.RESET);

            this.escapeCodeNameMapping = new Dictionary<string, string>();
            this.escapeCodeNameMapping.Add("0", Ansi.Style.RESET);
            this.escapeCodeNameMapping.Add("1", Ansi.Style.BOLD);
            this.escapeCodeNameMapping.Add("3", Ansi.Style.ITALIC);
            this.escapeCodeNameMapping.Add("4", Ansi.Style.UNDERLINE);
            this.escapeCodeNameMapping.Add("30", Ansi.Style.BLACK);
            this.escapeCodeNameMapping.Add("31", Ansi.Style.RED);
            this.escapeCodeNameMapping.Add("32", Ansi.Style.GREEN);
            this.escapeCodeNameMapping.Add("33", Ansi.Style.YELLOW);
            this.escapeCodeNameMapping.Add("34", Ansi.Style.BLUE);
            this.escapeCodeNameMapping.Add("35", Ansi.Style.MAGENTA);
            this.escapeCodeNameMapping.Add("36", Ansi.Style.CYAN);
            this.escapeCodeNameMapping.Add("37", Ansi.Style.WHITE);
            this.escapeCodeNameMapping.Add("40", Ansi.Style.BG_BLACK);
            this.escapeCodeNameMapping.Add("41", Ansi.Style.BG_RED);
            this.escapeCodeNameMapping.Add("42", Ansi.Style.BG_GREEN);
            this.escapeCodeNameMapping.Add("43", Ansi.Style.BG_YELLOW);
            this.escapeCodeNameMapping.Add("44", Ansi.Style.BG_BLUE);
            this.escapeCodeNameMapping.Add("45", Ansi.Style.BG_MAGENTA);
            this.escapeCodeNameMapping.Add("46", Ansi.Style.BG_CYAN);
            this.escapeCodeNameMapping.Add("47", Ansi.Style.BG_WHITE);

            this.nameActivatedMapping = new Dictionary<string, bool>();
            foreach(string val in this.escapeCodeNameMapping.Values)
            {
                if(!Ansi.Style.RESET.Equals(val))
                    this.nameActivatedMapping.Add(val, false);
            }

            

            this.ansiEscapeseqPattern = new Regex(
                "(\x1b\x5b[0-9:;<=>?][;|0-9:;<=>?]*[A-Z|a-z|\x40\x5b\x5c\x5d\x5e\x5f\x60\x7b\x7c\x7d\x7e])",
                RegexOptions.Compiled);
            this.oscPatter = new Regex(
                //@"\e\[.*\b", 
                "\x1b\x5b[\x00-\xff]*\x07",
                RegexOptions.Compiled);
        }

        //
        // source: https://en.wikipedia.org/wiki/ANSI_escape_code
        //
        // - 'ESC' '['  == 0x1B (27), 0x5b (91)   -> CSI (Control Sequence Introducer)
        //      second character may be in range 0x40–0x5F ('@' to '_' char)
        // - general structure of most ANSI escape sequences is 
        //     CSI [private mode character(s?)] n1 ; n2... [trailing intermediate character(s?)] letter
        // - final char specifies command (in range 0x40–0x7E (64–126) )
        //
        // Some ANSI control codes
        // - CSI n A            cursor up per n cells
        // - CSI n B            cursor down
        // - CSI n C            cursor Forward
        // - CSI n D            cursor Back
        // - CSI n E            cursor next line
        // - CSI n F            cursor previous line
        //
        // - CSI n J            erase display
        // - CSI n K            erase line
        // - CSI n S            scroll up
        // - CSI n T            scroll down
        // - CSI n;m f          HVP - Horizontal and Vertical Position, 
        //                          e.g. Moves the cursor to row n , column m
        // - CSI n m            SGR - Select Graphic Rendition
        //                        Sets SGR parameters, including text color.
        //                        After CSI can be zero or more parameters separated with ;. 
        //                        With no parameters, CSI m is treated as CSI 0 m (reset / normal), 
        //                        which is typical of most of the ANSI escape sequences.
        //
        // SGR (Select Graphic Rendition) parameters
        // - 0                  reset
        // - 1                  bold
        //
        // - 3                  Italic:on
        // - 4                  Underline: Single
        //
        // - 30 - 37            Textcolor with 30 + n, where n is a color from the table below
        // - 40 - 47            Background color (same as Textcolor)
        //
        // Colors
        // - 0                  Black
        // - 1                  Red
        // - 2                  Green
        // - 3                  Yellow
        // - 4                  Blue
        // - 5                  Magenta
        // - 6                  Cyan
        // - 7                  White
        //
        // Remarks: implement bold as brighter color - but can also use bold
        //
        public Tuple<string, List<Tuple<string, HashSet<string>>>> parseANSIEscapeString(string input)
        {
            // a list of result strings (which should be printed)
            //  and a HashSet containing the properties set (e.g. Ansi.Style.BOLD)
            List<Tuple<string, HashSet<string>>> resultLst
                = new List<Tuple<string, HashSet<string>>>();
            // contains the whole data without formatting information
            string rawData = "";

            bool matched = this.ansiEscapeseqPattern.IsMatch(input);
            if (matched)
            {                
                string[] result = this.ansiEscapeseqPattern.Split(input);
                foreach (string elem in result)
                {
                    byte[] bArr = Encoding.ASCII.GetBytes(elem);

                    // check for escape sequences - first byte must be 0x1b
                    //   and second byte '['
                    if(elem.Length > 2 && elem[0] == '\x1b' && elem[1] == '[')
                    {
                        // see if this is an escape code for SGR parameters
                        if(elem[elem.Length-1] == 'm')
                        {
                            // remove first 2 and last element in string
                            string elemStripped = elem.Substring(2, elem.Length-3);
                            if(elemStripped.Length == 0)
                            {
                                // RESET
                                this.resetNameActivatedMapping();
                            }
                            else
                            {
                                // split at ';'
                                string[] ret = elemStripped.Split(';');
                                foreach(string x in ret)
                                {
                                    string number = new string(x.ToCharArray());
                                    number = number.TrimStart('0');
                                    if (number.Equals("")) number = "0";
                                    string escapeCodeName;
                                    bool found = this.escapeCodeNameMapping.TryGetValue(number, out escapeCodeName);
                                    if(found)
                                    {
                                        if(Ansi.Style.RESET.Equals(escapeCodeName))
                                        {
                                            this.resetNameActivatedMapping();
                                        }
                                        else
                                        {
                                            this.nameActivatedMapping[escapeCodeName] = true;
                                        }                                        
                                    }
                                }
                            }                            

                        }
                        // TODO here check other stuff, e.g. cursor movement
                        //  - but cursor movement must come from the client side?
                        else
                        {
                            // unknown escape code found
                            LoggingHelper.writeLogEntryLine(this.GetType().Name,
                               "Warning unkown escape code found: " + elem +
                                ", in hex=0x" + BitConverter.ToString(bArr));
                        }
                    }
                    else
                    {
                        Tuple<string, HashSet<string>> tup = createNewDataPropertiesTuple(elem);
                        if(tup != null)
                        {
                            // Add a new normal string to the result list
                            resultLst.Add(tup);
                            // add string to rawdata
                            rawData += tup.Item1;
                        }                        
                    }
                }
            }
            else
            {

                // Add a the input string to the result list
                // in this case it will only contain this element
                Tuple<string, HashSet<string>> tup = createNewDataPropertiesTuple(input);
                if(tup != null)
                {
                    resultLst.Add(tup);
                    rawData = tup.Item1;
                }                
            }

            return new Tuple<string, List<Tuple<string, HashSet<string>>>> (rawData, resultLst);
        }

        public string removeUnwantedCharacters(string input)
        {
            return input.Replace("\x07", "");
        }

        private void resetNameActivatedMapping()
        {
            foreach(string elem in this.nameActivatedMapping.Keys.ToList())
            {
                this.nameActivatedMapping[elem] = false;
            }
        }

        private Tuple<string, HashSet<string>> createNewDataPropertiesTuple(string data)
        {
            data = this.oscPatter.Replace(data, "");
            
            // Here a normal string must be handled according to the current
            // nameActivatedMapping - the actual handling must be done by the application using
            // this ANSIEscapeCodeHelper
            HashSet<string> propertySet = new HashSet<string>();
            foreach (string prop in this.nameActivatedMapping.Keys)
            {
                if (this.nameActivatedMapping[prop])
                {
                    propertySet.Add(prop);
                }
            }

            // Here before adding the data string unwanted characters can/should be removed
            // (e.g. non printable chars)
            // char 0x07 is the BELL char aka '\a'  
            //   --> see https://msdn.microsoft.com/en-us/library/h21280bw.aspx
            data = data.Replace("\x07", "").Replace("\b", "");

            if(data.Equals(""))
            {
                return null;
            }
            else
            {
                return new Tuple<string, HashSet<string>>(data, propertySet);
            }            
        }

    }
}
