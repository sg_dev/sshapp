﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalSSHApp.Utils
{
    public static class SSHHelper
    {
        public static SshClient SSHClient { get; private set; }
        public static ShellStream ShellStream { get; private set; }

        public static void EstablishSSHConnection(Connection con)
        {            
            try
            {
                SSHClient = new SshClient(
                    con.Host, con.Port, con.Username, con.Password);
                SSHClient.Connect();
                // can also try other terminals (like xterm etc.)
                ShellStream = SSHClient.CreateShellStream("xterm", 0, 0, 0, 0, 1024);
            }
            catch (Exception ex)
            {
                if (ex is System.Net.Sockets.SocketException)
                {
                    LoggingHelper.writeLogEntryLine(typeof(SSHHelper).Name,
                    "SocketException: " + ex);
                    throw new ApplicationException("Network Error: Check if you are connected to the internet!\n" +
                        "Also the remote host could be down!", ex);
                }

                if (ex is Renci.SshNet.Common.SshConnectionException)
                {
                    LoggingHelper.writeLogEntryLine(typeof(SSHHelper).Name, 
                        "Renci.SshNet.Common.SshConnectionException: " + ex);
                    throw new ApplicationException("Application Error: Connection terminated!", ex);
                }

                if (ex is Renci.SshNet.Common.SshAuthenticationException)
                {
                    LoggingHelper.writeLogEntryLine(typeof(SSHHelper).Name, 
                        "Renci.SshNet.Common.SshAuthenticationException: " + ex);
                    throw new ApplicationException("Authentication Error: Provided credentials/private key file is invalid!", ex);
                }

                LoggingHelper.writeLogEntryLine(typeof(SSHHelper).Name, 
                    "Exception: " + ex);
                throw;
            }
        }

        public static void shutdownCurrentConnection()
        {
            LoggingHelper.writeLogEntryLine(typeof(SSHHelper).Name,
                "Shutdown the SSHHelper!");

            ShellStream.Dispose();
            SSHClient.Disconnect();
            SSHClient.Dispose();

            ShellStream = null;
            SSHClient = null;
        }

        
    }
}
