﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalSSHApp.Utils
{
    class HistoryHelper
    {
        private static readonly int THRESHOLD = 1000;
        private static readonly int REMOVE_END_INDEX = 500;

        private Object thisLock = new Object();
        private List<string> history;
        private int historyPtr;

        public HistoryHelper()
        {
            this.history = new List<string>();
        }

        public async void addToHistory(string command)
        {
            lock(thisLock)
            {
                this.history.Add(command);
                this.historyPtr = this.history.Count - 1;
            }

            await Task.Run(() => this.cleanList());
        }

        public string getPreviousCommand()
        {
            string ret;
            lock(thisLock)
            {
                ret = this.history[this.historyPtr];
                if(this.historyPtr > 0)
                    this.historyPtr--;
            }

            return ret;
        }

        public string getNextCommand()
        {
            string ret;
            lock (thisLock)
            { 
                int nextInd = this.historyPtr < this.history.Count - 1 ? this.historyPtr++ : this.historyPtr;
                ret = this.history[nextInd];
            }

            return ret;
        }

        private void cleanList()
        {
            List<string> workingList = this.history.ToList();

            if (this.history.Count > THRESHOLD)
            {
                workingList.RemoveRange(0, REMOVE_END_INDEX);
            }

            lock (thisLock)
            {
                this.history = workingList;
                this.historyPtr = this.history.Count - 1;
            }
        }

    }
}
