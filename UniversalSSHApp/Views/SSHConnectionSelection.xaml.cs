﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UniversalSSHApp.Utils;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalSSHApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SSHConnectionSelection : Page
    {
        public SSHConnectionSelection()
        {
            this.InitializeComponent();
            Loaded += ConnectionSelection_Loaded;
        }

        private async void ConnectionSelection_Loaded(object sender, RoutedEventArgs e) {
            await ConnectionHelper.LoadConnectionListAsync();

            if(ConnectionHelper.getConnectionList().Count == 0)
            {
                Frame.Navigate(typeof(Login));
            }

            ConnectionListView.ItemsSource = ConnectionHelper.getConnectionList();
            ConnectionListView.SelectionChanged += ConnectionSelectionChanged;
        }

        private void ConnectionSelectionChanged(object sender, RoutedEventArgs e) {
            if (((ListView)sender).SelectedValue != null) {
                Connection con = (Connection)((ListView)sender).SelectedValue;
                if(con != null)
                {
                    LoggingHelper.writeLogEntryLine(this.GetType().Name, 
                        "Connection to " + con.Host + 
                        ":" + con.Port + " with username " + con.Username + " selected!");
                    
                    try
                    {
                        SSHHelper.EstablishSSHConnection(con);
                        LoggingHelper.writeLogEntryLine(this.GetType().Name,
                            "Connection to " + con.Host + ":" + con.Port + " established!");
                        this.Frame.Navigate(typeof(SSHPage));
                    }
                    catch(ApplicationException ex)
                    {
                        this.ErrorMessage.Text = ex.Message;
                    }
                    catch(Exception ex)
                    {
                        LoggingHelper.writeLogEntryLine(this.GetType().Name, 
                            "Exception: " + ex);
                    }                    
                }
            }
        }

        public void AddConnectionButton_Click(object sender, RoutedEventArgs e) {
            this.Frame.Navigate(typeof(Login));
        }
    }
}
