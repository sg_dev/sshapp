﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Renci.SshNet;
using UniversalSSHApp.Utils;
using Windows.UI.Core;
using System.Threading.Tasks;
using System.Text;
using System.Text.RegularExpressions;
using Windows.UI.Xaml.Documents;
using Windows.UI.Text;
using Windows.UI;
using Windows.Foundation.Metadata;

namespace UniversalSSHApp.Views
{
    
    public sealed partial class SSHPage : Page
    {
        private bool initialized;
        // used to check if we are on a mobile device
        private bool isHardwareButtonsAPIPresent;
        private int input_send_index;
        private int data_read_index;
        private string data_read;
        private bool carriageReturnLineFeedArrived;
        private Regex cmdPrompt;
        private ANSIEscapeCodeHelper escapeCodeHelper;
        private HistoryHelper historyHelper;

        public SSHPage() {
            this.InitializeComponent();

            CoreWindow.GetForCurrentThread().KeyDown += KeyDownHandler;
            this.input_send_index = 0;
            this.data_read_index = 0;
            this.data_read = "";
            this.carriageReturnLineFeedArrived = false;
            this.cmdPrompt = new Regex(@"\b[a-zA-z0-9]+@[a-zA-z0-9]+:.*[\$#]", 
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            this.escapeCodeHelper = new ANSIEscapeCodeHelper();
            this.historyHelper = new HistoryHelper();

            //
            // Only add command bar when we are on a mobile device
            //
            this.isHardwareButtonsAPIPresent =
                ApiInformation.IsTypePresent("Windows.Phone.UI.Input.HardwareButtons");

            if(this.isHardwareButtonsAPIPresent)
            {
                CommandBar cmdBar = new CommandBar();
                this.sshPage.BottomAppBar = cmdBar;

                AppBarButton b = new AppBarButton();
                b.Label = "Tab";
                FontIcon f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "\u21b9";
                b.Icon = f;
                b.Click += this.TabButtonClickHandler;               

                AppBarButton b1 = new AppBarButton();
                b1.Label = "Up Arrow";
                f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "\u2191";
                b1.Icon = f;
                b1.Click += this.UpButtonClickHandler;

                AppBarButton b2 = new AppBarButton();
                b2.Label = "Down Arrow";
                f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "\u2193";
                b2.Icon = f;
                b2.Click += this.DownButtonClickHandler;

                AppBarButton b3 = new AppBarButton();
                b3.Label = "/";
                f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "/";
                b3.Icon = f;
                b3.Click += (s, args) =>
                {
                    this.Input_Send.Text += "/";
                    this.Input_Send.Select(this.Input_Send.Text.Length, 0);
                };

                AppBarButton b4 = new AppBarButton();
                b4.Label = "-";
                f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "-";
                b4.Icon = f;
                b4.Click += (s, args) =>
                {
                    this.Input_Send.Text += "-";
                    this.Input_Send.Select(this.Input_Send.Text.Length, 0);
                };

                AppBarButton b5 = new AppBarButton();
                b5.Label = "Enter";
                f = new FontIcon();
                f.FontFamily = new FontFamily("Candara");
                f.Glyph = "\u21b5"; // u21a9
                b5.Icon = f;
                b5.Click += EnterButtonClickHandler;

                cmdBar.PrimaryCommands.Add(b);
                cmdBar.PrimaryCommands.Add(b3);
                cmdBar.PrimaryCommands.Add(b4);
                cmdBar.PrimaryCommands.Add(b5);
                
                cmdBar.PrimaryCommands.Add(b1); // maybe also move the Up arrow into the overflow menu
                cmdBar.SecondaryCommands.Add(b2);

                //
                // on mobile devices we add newlines on the bottom s.t 
                //   the one screen keyboard does not overlapp with the output
                // Note: it must be asserted that this Run element is always the last
                //   element in the SSHView.Inlines Collection
                //
                String newlineString = new String('\n', 12);
                Run r = new Run();
                r.Text = newlineString;
                this.SSHView.Inlines.Add(r);

                Windows.UI.ViewManagement.InputPane.GetForCurrentView().Showing += async (s, args) =>
                {
                    // 284 on my windows phone
                    // 215.33 in emulator
                    // -> 17.94 per char
                    const double heightPerChar = 17.94;
                    double height = args.OccludedRect.Height;
                    int charsNeeded = (int)Math.Round(height / heightPerChar, 0);
                    LoggingHelper.writeLogEntryLine(this.GetType().Name,
                        "height=" + height + ", charsNeeded=" + charsNeeded);

                    Inline il = this.SSHView.Inlines[this.SSHView.Inlines.Count - 1];
                    if (typeof(Run) == il.GetType())
                    {
                        Run run = (Run)il;
                        int textlength = run.Text.Length;
                        if (textlength != charsNeeded)
                        {
                            LoggingHelper.writeLogEntryLine(this.GetType().Name,
                                "Updating the last Run element to have " + charsNeeded + " newlines!");

                            String inlineStr = new String('\n', charsNeeded);
                            run.Text = inlineStr;

                            await Task.Delay(5);
                            this.ViewPort.ChangeView(0.0f, double.MaxValue, 1.0f);
                        }
                    }
                    else
                    {
                        LoggingHelper.writeLogEntryLine(this.GetType().Name,
                            "Error: Typeof last element in SSHView.Inlines must be Run");
                    }
                };

                Windows.UI.ViewManagement.InputPane.GetForCurrentView().Hiding += (s, args) =>
                {
                    // here the size of the last Run element could be resetted (but not needed)
                };
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            if(initialized == false)
            {
                var window = Windows.UI.Core.CoreWindow.GetForCurrentThread();
                Windows.UI.Core.CoreDispatcher guiDispatcher = window.Dispatcher;

                SSHHelper.ShellStream.DataReceived += async (sender, dataEvent) =>
                {
                    string data = SSHHelper.ShellStream.Read();
                    Tuple < string, List < Tuple < string, HashSet < string >>>> parsingResult =
                          this.escapeCodeHelper.parseANSIEscapeString(data);
                    string rawData = parsingResult.Item1;
                    List < Tuple < string, HashSet < string >>> dataPropMapping =
                        parsingResult.Item2;

                    await guiDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            // here translate the ANSIEscapeCodeHelper format to be diplayed in the UI
                            this.addTextElementToSSHView(dataPropMapping);

                            if(this.input_send_index != 0)
                            {

                                // If something appears below the prompt, e.g. some suggestions 
                                //   -> they must not appear in the input field!
                                // Since more lines may belong to the output 
                                //   (which must not be copied to the user input field)
                                //   search for the next bash promt line, e.g. 'user@host:~$'
                                // Note: the promt will be '$' for ordinary users and '#' for root
                                // see https://www.digitalocean.com/community/tutorials/how-to-customize-your-bash-prompt-on-a-linux-vps
                                //
                                if (this.carriageReturnLineFeedArrived)
                                {
                                    if(this.cmdPrompt.IsMatch(rawData))
                                    {
                                        this.carriageReturnLineFeedArrived = false;
                                    }
                                    
                                    return;
                                }
                                if (rawData.Equals("\x0d\x0a")) // carriage return, line feed
                                {
                                    this.carriageReturnLineFeedArrived = true;
                                    return;
                                }
                                else
                                {
                                    this.carriageReturnLineFeedArrived = false;
                                }                                


                                this.data_read_index += rawData.Length;
                                this.data_read += rawData;                                

                                if (this.data_read_index > this.input_send_index)
                                {
                                    // here data_read must not contain any non printable character, e.g. the BELL char
                                    // else there won't be appended anything to the TextBox
                                    this.Input_Send.Text += this.data_read.Substring(this.input_send_index);
                                    this.input_send_index = this.Input_Send.Text.Length;

                                    // move cursor to end of textbox
                                    this.Input_Send.Select(this.Input_Send.Text.Length, 0);
                                }
                            }

                            // https://stackoverflow.com/questions/11171456/best-way-to-scroll-to-end-of-scrollviewer
                            // https://code.msdn.microsoft.com/windowsapps/How-to-scroll-to-the-a8ea5867
                            await Task.Delay(5);                            
                            this.ViewPort.ChangeView(0.0f, double.MaxValue, 1.0f);
                        }
                    );

                };

                initialized = true;
            }
        }

        private void KeyDownHandler(CoreWindow sender, KeyEventArgs args)
        {  
            
            if(args.VirtualKey == Windows.System.VirtualKey.Enter)
            {
                this.EnterButtonClickHandler(null, null);             

            }
            else if(args.VirtualKey == Windows.System.VirtualKey.Tab)
            {

                this.TabButtonClickHandler(null, null);

            }
            else if(args.VirtualKey == Windows.System.VirtualKey.Back)
            {                
                //
                // Handle text deletion with Back key
                //                
                if(this.input_send_index != 0) // only if previously a partially command was sent (using \t)
                {
                    this.delete1Character();
                }                
            }
            //
            // History
            //
            else if(args.VirtualKey == Windows.System.VirtualKey.Up)
            {
                this.UpButtonClickHandler(null, null);
            }
            else if (args.VirtualKey == Windows.System.VirtualKey.Down)
            {
                this.DownButtonClickHandler(null, null);
            }
        }

        private void EnterButtonClickHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                SSHHelper.ShellStream.WriteLine(this.Input_Send.Text.Substring(this.input_send_index));
                this.historyHelper.addToHistory(this.Input_Send.Text);
                //
                // here reset everything
                //
                this.Input_Send.Text = "";
                this.input_send_index = 0;
                this.data_read_index = 0;
                this.data_read = "";
                this.carriageReturnLineFeedArrived = false;
            }
            catch (Exception ex)
            {
                if (ex is ObjectDisposedException)
                {
                    LoggingHelper.writeLogEntryLine(this.GetType().Name,
                        "ObjectDisposedException ex: " + ex);
                    this.Input_Send.Text = "";
                    // TODO better show here a dialog instead
                    Run r = new Run();
                    r.Text = "\n\nError: Connection is already closed!\n";
                    if (this.isHardwareButtonsAPIPresent)
                    {
                        this.SSHView.Inlines.Insert(this.SSHView.Inlines.Count - 1, r);
                    }
                    else
                    {
                        this.SSHView.Inlines.Add(r);
                    }

                    return;
                }

                if (ex is Renci.SshNet.Common.SshConnectionException)
                {
                    LoggingHelper.writeLogEntryLine(this.GetType().Name,
                        "Renci.SshNet.Common.SshConnectionException: " + ex);
                    this.Input_Send.Text = "";
                    // TODO better show here a dialog instead
                    Run r = new Run();
                    r.Text = "\n\nError: Connection is already closed!\n";
                    if (this.isHardwareButtonsAPIPresent)
                    {
                        this.SSHView.Inlines.Insert(this.SSHView.Inlines.Count - 1, r);
                    }
                    else
                    {
                        this.SSHView.Inlines.Add(r);
                    }

                    return;
                }

                LoggingHelper.writeLogEntryLine(this.GetType().Name,
                    "Exception: " + ex);
                throw;
            }
        }

        private void TabButtonClickHandler(object sender, RoutedEventArgs e)
        {
            SSHHelper.ShellStream.Write(this.Input_Send.Text.Substring(this.input_send_index) + "\t");
            this.input_send_index = this.Input_Send.Text.Length;
        }

        private void UpButtonClickHandler(object sender, RoutedEventArgs e)
        {
            if (this.input_send_index != 0)
            {
                this.deleteNCharacters(this.input_send_index);
                this.input_send_index = 0;
            }

            this.Input_Send.Text = this.historyHelper.getPreviousCommand();
            // move cursor to end of textbox
            this.Input_Send.Select(this.Input_Send.Text.Length, 0);
        }

        private void DownButtonClickHandler(object sender, RoutedEventArgs e)
        {
            if (this.input_send_index != 0)
            {
                this.deleteNCharacters(this.input_send_index);
                this.input_send_index = 0;
            }
            this.Input_Send.Text = this.historyHelper.getNextCommand();
            // move cursor to end of textbox
            this.Input_Send.Select(this.Input_Send.Text.Length, 0);
        }

        private static readonly SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
        private static readonly SolidColorBrush redBrush = new SolidColorBrush(Colors.Red);
        private static readonly SolidColorBrush greenBrush = new SolidColorBrush(Colors.Green);
        private static readonly SolidColorBrush yellowBrush = new SolidColorBrush(Colors.Yellow);
        private static readonly SolidColorBrush blueBrush = new SolidColorBrush(Colors.Blue);
        private static readonly SolidColorBrush magentaBrush = new SolidColorBrush(Colors.Magenta);
        private static readonly SolidColorBrush cyanBrush = new SolidColorBrush(Colors.Cyan);
        private static readonly SolidColorBrush whiteBrush = new SolidColorBrush(Colors.White);
        private void addTextElementToSSHView(List<Tuple<string, HashSet<string>>> input)
        {
            // first check if list is empty
            if (input.Count == 0)
                return;

            Span s = new Span();
            foreach(Tuple<string, HashSet<string>> elem in input)
            {
                Run r = new Run();
                r.Text = elem.Item1;
                r.FontSize = 14;

                HashSet<string> propertiesSet = elem.Item2;
                if(propertiesSet.Contains(Utils.Ansi.Style.BOLD))
                {
                    r.FontWeight = FontWeights.Bold;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.ITALIC))
                {
                    r.FontStyle = FontStyle.Italic;
                }
                if(propertiesSet.Contains(Utils.Ansi.Style.UNDERLINE))
                {
                    // use bold instead of underline
                    r.FontWeight = FontWeights.Bold;
                }

                //
                // foreground colors
                //
                if(propertiesSet.Contains(Utils.Ansi.Style.BLACK))
                {
                    r.Foreground = blackBrush;
                }
                if(propertiesSet.Contains(Utils.Ansi.Style.RED))
                {
                    r.Foreground = redBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.GREEN))
                {
                    r.Foreground = greenBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.YELLOW))
                {
                    r.Foreground = yellowBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BLUE))
                {
                    r.Foreground = blueBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.MAGENTA))
                {
                    r.Foreground = magentaBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.CYAN))
                {
                    r.Foreground = cyanBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.WHITE))
                {
                    r.Foreground = whiteBrush;
                }

                //
                // background colors
                //
                if(propertiesSet.Contains(Utils.Ansi.Style.BG_BLACK))
                {
                    this.Frame.Background = blackBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_RED))
                {
                    this.Frame.Background = redBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_GREEN))
                {
                    this.Frame.Background = greenBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_YELLOW))
                {
                    this.Frame.Background = yellowBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_BLUE))
                {
                    this.Frame.Background = blueBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_MAGENTA))
                {
                    this.Frame.Background = magentaBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_CYAN))
                {
                    this.Frame.Background = cyanBrush;
                }
                if (propertiesSet.Contains(Utils.Ansi.Style.BG_WHITE))
                {
                    this.Frame.Background = whiteBrush;
                }


                s.Inlines.Add(r);
            }
            if(this.isHardwareButtonsAPIPresent)
            {
                this.SSHView.Inlines.Insert(this.SSHView.Inlines.Count - 1, s);
            }
            else
            {
                this.SSHView.Inlines.Add(s);
            }            
        }

        private void deleteNCharacters(int n)
        {
            // use ASCII DEL (0x7f) for character delete
            // see http://www.ibb.net/~anne/keyboard.html
            String deleteString = new String('\x7f', n);

            SSHHelper.ShellStream.Write(deleteString);
            this.input_send_index -= n;
            this.data_read_index -= n;
            this.data_read = this.data_read.Remove(this.data_read.Length - n);

            // recursively delete the last n output elements
            this.deleteNCharsInOutput(n);            
        }

        //
        // This recursive method assumes that the SSHView.Inlines list only contains
        // Span elements
        // and each Span elements only contains Run elements
        // It is also assumed that the Text property of each Run element contains at
        // least one character
        //
        private void deleteNCharsInOutput(int n)
        {
            Inline il;
            if(this.isHardwareButtonsAPIPresent)
            {
                il = this.SSHView.Inlines[this.SSHView.Inlines.Count - 2];
            }
            else
            {
                il = this.SSHView.Inlines[this.SSHView.Inlines.Count - 1];
            }

            if (il.GetType() == typeof(Span))
            {
                Span s = (Span)il;
                Inline innerInline = s.Inlines[s.Inlines.Count - 1];
                if (innerInline.GetType() == typeof(Run))
                {
                    Run r = (Run)innerInline;
                    int textlength = r.Text.Length;
                    if (textlength > n)
                    {
                        r.Text = r.Text.Remove(textlength - n);
                    }
                    else // textlength <= n
                    {
                        if (s.Inlines.Count > 1)
                        {
                            // remove Run from the Span
                            s.Inlines.RemoveAt(s.Inlines.Count - 1);
                        }
                        else
                        {
                            // since the Span would be empty it can be removed completely
                            if(this.isHardwareButtonsAPIPresent)
                            {
                                this.SSHView.Inlines.RemoveAt(this.SSHView.Inlines.Count - 2);
                            }
                            else
                            {
                                this.SSHView.Inlines.RemoveAt(this.SSHView.Inlines.Count - 1);
                            }                            
                        }

                        // Now here we would be done if textlength == n
                        if (textlength < n)
                        {
                            // additional work to do since we have not removed everything
                            this.deleteNCharsInOutput(n - textlength);
                        }
                    }
                }
                else
                {
                    LoggingHelper.writeLogEntryLine(this.GetType().Name, 
                        "Error: Invalid Inline element found: " + innerInline);
                }
            }
            else
            {
                LoggingHelper.writeLogEntryLine(this.GetType().Name, 
                    "Error: Invalid Inline element found: " + il);
            }
        }

        private void delete1Character()
        {
            this.deleteNCharacters(1);
        }
    }
}
