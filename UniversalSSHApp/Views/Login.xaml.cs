﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Renci.SshNet;
using UniversalSSHApp.Utils;

namespace UniversalSSHApp.Views
{
    
    public sealed partial class Login : Page
    {
        public Login() {
            this.InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs args) {
            this.ErrorMessage.Text = "";

            try
            {
                Connection con = ConnectionHelper.AddConnection(this.HostTextBox.Text, this.PortTextBox.Text,
                    this.UsernameTextBox.Text, this.PasswordBox.Password);
                SSHHelper.EstablishSSHConnection(con);
                // now save the connectionlist
                ConnectionHelper.SaveConnectionsListAsync();

                this.Frame.Navigate(typeof(SSHPage));
            }
            catch(ApplicationException ex)
            {
                this.ErrorMessage.Text = ex.Message;
            }
        }
    }
}
