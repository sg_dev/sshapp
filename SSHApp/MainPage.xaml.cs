﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Renci.SshNet;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Text;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SSHApp
{

    public sealed partial class MainPage : Page {

        public MainPage() {
            this.InitializeComponent();
        }

        private void Button1_Click_1(object sender, RoutedEventArgs e) {
            string ip = "192.168.1.99";
            int port = 22;
            string username = this.Username.Text;
            string password = this.Password.Password;

            //           send_cmd_example(ip, port, username, password);
            event_test(ip, port, username, password);
        }

        private ShellStream shellStream;
        private void event_test(string ip, int port, string user, string pass) {
            this.Result1.Text = "";

            try {
                SshClient client = new SshClient(ip, port, user, pass);
                client.Connect();
                System.Diagnostics.Debug.WriteLine("Connected!");
                var window = Windows.UI.Core.CoreWindow.GetForCurrentThread();
                Windows.UI.Core.CoreDispatcher guiDispatcher = window.Dispatcher;

                this.shellStream = client.CreateShellStream("vanilla", 0, 0, 0, 0, 1024);

                this.shellStream.DataReceived += async (sender, dataEvent) =>
                {
                    string data = this.shellStream.Read();
                    await guiDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        () =>
                        {
                            this.Result1.Text += data;
                        }
                    );

                };
            }
            catch(Exception ex) {
                System.Diagnostics.Debug.WriteLine(
                   "SocketException: " + ex + "; msg:" + ex.Message + "\n");
            }
        }

        private void Send_Click(object sender, RoutedEventArgs e) {
            this.shellStream.WriteLine(this.Input_Send.Text);
            this.Input_Send.Text = "";
        }

        private void send_cmd_example(string ip, int port, string user, string pass) {
            System.Diagnostics.Debug.WriteLine("Setting up connection info...");

            ConnectionInfo cInfo = new ConnectionInfo(ip, port, user,
                new AuthenticationMethod[]{
                    // Pasword based Authentication
                    new PasswordAuthenticationMethod(user, pass),

                    /*
                    // Key Based Authentication (using keys in OpenSSH Format)
                    new PrivateKeyAuthenticationMethod("username",new PrivateKeyFile[]{
                        new PrivateKeyFile(@"..\openssh.key","passphrase")
                    }),
                    */
                }
            );

            System.Diagnostics.Debug.WriteLine("Trying to execute command...");

            try
            {
                using (var sshclient = new SshClient(cInfo))
                {
                    sshclient.Connect();
                    using (var cmd =
                        sshclient.CreateCommand(
                            "mkdir -p /tmp/uploadtest && chmod +rw /tmp/uploadtest"))
                    {
                        cmd.Execute();
                        System.Diagnostics.Debug.WriteLine("Command>" + cmd.CommandText);
                        System.Diagnostics.Debug.WriteLine("Return Value = {0}", cmd.ExitStatus);
                    }
                    sshclient.Disconnect();
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(
                    "SocketException: " + ex + "; msg:" + ex.Message + "\n");
            }
        }
    }
}
