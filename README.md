# README #

## Quick summary ##

This is a small SSH Client Windows Universal App.

The app is in the **UniversalSSHApp** directory, 
*SSHApp* contains a simple test app for demonstrating Renci.SshNet library's functionality and
*WindowsUniversalAppTests* contains some Windows Universal App demo code.


### Dependencies ###
* SSH.Net library (https://sshnet.codeplex.com/)

## Development TODOs ##

* Add Buttons for Mobile: '/', '-', shutdown (using `sudo shutdown -h now`)
* Add support for interactive programms (e.g. nano) --> **ANSI Escape codes
**

## Issues ##

currently None.


## Who do I talk to? ##

* Repo owner